package WWW::OrangeHRM::Client;
use strict;
use warnings;
use utf8;
use version 0.77; our $VERSION = qv("v0.12.0");

use WWW::Mechanize;
use HTML::TreeBuilder::LibXML;
use DateTime;
use DateTime::Duration;

use Exporter;
our @ISA = qw(Exporter);
our @EXPORT_OK = qw(debug_http dump_state fatal_error log_in log_out time_sheet
    time_sheet_change time_sheet_date time_sheet_year_month time_sheet_show
    normalize_duration normalize_time string2time string2time_duration
    time_duration2string time2string time_sheet_parse time_sheet_save
    time_sheet_set time_sheet_set_day time_sheet_submit year_month2date);

=encoding utf8

=head1 NAME

WWW::OrangeHRM::Client - Client for OrangeHRM

=head1 DESCRIPTION

This module implements client for
L<OrangeHRM|http://en.wikipedia.org/wiki/OrangeHRM> information system. It has
been developed against Red Hat instance, so I cannot guarantee it works with
other instances. Author and this code have no business or personal relation to
OrangeHRM Inc.

=head1 SYNOPSIS

    use WWW::OrangeHRM::Client;
    my %configuration = (
        url => 'https://redhat.orangehrm.com/',
        samlidp => 'https://saml.redhat.com/',
        samlout => 'https://example.redhat.com/bye',
        username => 'foo',
        password => 'bar'
    );
    my $debug = 0;
    my $automaton = WWW::Mechanize->new();
    
    # Log-in
    if (!WWW::OrangeHRM::Client::log_in($automaton, \%configuration)) {
        WWW::OrangeHRM::Client::fatal_error($automaton, $debug,
            'Could not log in!');
    }
    print "Logged in.\n";
    
    if (!WWW::OrangeHRM::Client::time_sheet($automaton)) {
        WWW::OrangeHRM::Client::fatal_error($automaton, $debug,
            'Could not get time sheet!');
    }
    
    WWW::OrangeHRM::Client::time_sheet_show($automaton);
    
    # Log-out
    if (!WWW::OrangeHRM::Client::log_out($automaton)) {
        WWW::OrangeHRM::Client::fatal_error($automaton, $debug,
            'Could not log out!');
    }
    print "Logged out.\n";

=cut

my $last_request;

BEGIN {
    $last_request = undef;
}


=head1 API

$automaton is C<WWW::Mechanize> object.

=head2 %configuration

    my %configuration = (
        url => 'https://redhat.orangehrm.com/',
        samlidp => 'https://saml.redhat.com/',
        samlout => 'https://example.redhat.com/bye',
        username => 'foo',
        password => undef
    );

=cut 

=head2 debug_http($automaton)

Call this after creating $automaton if you want to see HTTP request in
dump_state() or fatal_error() dump.

=cut

sub debug_http {
    my $automaton = shift;
    $automaton->add_handler("request_send", sub {
            $last_request = shift->dump(maxlength => 0);
            return;
        });
    1;
}


=head2 dump_state($automaton)

Print details about last request and response.

=cut

sub dump_state {
    my $automaton = shift;

    if (defined $last_request) {
        print "===BEGIN HTTP REQUEST===\n";
        print $last_request;
        print "===END HTTP REQUEST===\n";
    }

    print 'Current URI: ', $automaton->uri, "\n";

    print 'Response status: ', $automaton->status, "\n";

    print "===BEGIN RESPONSE HEADERS===\n";
    $automaton->dump_headers();
    print "===END RESPONSE HEADERS===\n";

    print "===BEGIN RESPONSE BODY===\n";
    print $automaton->response->decoded_content, "\n";
    print "===END RESPONSE BODY===\n";
    
    print "===BEGIN TEXT DUMP===\n";
    $automaton->dump_text();
    print "===END TEXT DUMP===\n";
    
    print "===BEGIN LINK DUMP===\n";
    $automaton->dump_links();
    print "===END LINK DUMP===\n";
    
    print "===BEGIN FORM DUMP===\n";
    $automaton->dump_forms();
    print "===END FORM DUMP===\n";

    return 1;
}


=head2 fatal_error($automaton, $debug, $message)

Call this function, if you encounter fatal error.

$message will be printed. Automaton state will be dumped if $debug is true.

This function does not return, it exits program with an error exit code.

=cut

sub fatal_error {
    my ($automaton, $debug, $message) = @_;

    print $message, "\n";
    if ($debug) {
        dump_state($automaton);
        print $message, "\n";
    }
    
    exit 1;
}


=head2 get_text_status($automaton)

Return text in *[@id='messageBalloonContainer']/div element of current HTML
document or undef.

The @id specified element exists always. It always contains white space
text node. But the error message is stored in I<div> sub-element children and
the sub-element exists when reporting error.

User interface widgets mixed into the status message are ignored.

=cut

sub get_text_status {
    my $automaton = shift;
    my $document = $automaton->response->decoded_content;
    my $parser = HTML::TreeBuilder::LibXML->new()->parse_content($document)
        or return undef;
    my $nodes = $parser->findnodes(q{//*[@id="messageBalloonContainer"]/div});
    # $parser->findvalue() returns q{} if XPath does not match. There is no
    # way how to distinguish from element-only subtree. Therefore use size().
    my $text;
    if ($nodes->size > 0) {
        # Skip `×' anchors to javasrcipt widgets
        for my $div ($nodes->get_nodelist) {
            for my $button ($div->findnodes(q{a[@aria-label]})) {
                $div->removeChild($button);
            }
        }
        $text = $nodes->to_literal;
    }
    return $text;
}


=head2 get_maintenance_status($automaton)

Return text in /html/body/h2 element of current HTML document or return undef.

If time sheet sub-system is down, server returns 200, but there is a h2 element
just under body instead of any form or tabular data.

=cut

sub get_maintenance_status {
    my $automaton = shift;
    my $document = $automaton->response->decoded_content;
    my $parser = HTML::TreeBuilder::LibXML->new()->parse_content($document)
        or return undef;
    my $nodes = $parser->findnodes(q{/html/body/h2});
    # $parser->findvalue() returns q{} if XPath does not match. There is no
    # way how to distinguish from element-only subtree. Therefore use size().
    my $text;
    if ($nodes->size > 0) {
        $text = $nodes->to_literal;
    }
    return $text;
}


=head2 get_saml_status($automaton)

Return text in /html/body/section/div element of current HTML document or undef.

=cut

sub get_saml_status {
    my $automaton = shift;
    my $document = $automaton->response->decoded_content;
    my $parser = HTML::TreeBuilder::LibXML->new()->parse_content($document)
        or return undef;
    my $nodes = $parser->findnodes(q{/html/body/section/div});
    # $parser->findvalue() returns q{} if XPath does not match. There is no
    # way how to distinguish from element-only subtree. Therefore use size().
    my $text;
    if ($nodes->size > 0) {
        $text = $nodes->to_literal;
    }
    return $text;
}

=head2 press_continue_saml_button($automaton)

A private function.

Press a CONTINUE button in the only form of the page and check we get
successfully a new page. Return true on success, otherwise false.

=cut

sub press_continue_saml_button {
    my $automaton = shift;

    # Check the expected CONTINUE button is there. Otherwise some IDP fault
    # occured and clic_button() would die.
    my $form = $automaton->form_number(0) or return 0;
    my $submit_button = $form->find_input(undef, 'submit') or return 0;
    if ($submit_button->value ne 'CONTINUE') {
        my $message = get_saml_status($automaton);
        if (defined $message) {
            printf "SAML IDP reports: $message\n";
        }
        return 0;
    }

    # Press the CONTINUE button
    $automaton->click_button(input => $submit_button);
    if (!$automaton->success) {
        my $message = get_saml_status($automaton);
        if (defined $message) {
            printf "SAML IDP reports: $message\n";
        }
        return 0;
    }

    return 1;
}

=head2 follow_finishlogin_saml_link($automaton)

A private function.

Follow a //a[@id="finishLoginLink"] link a form-less page and check we get
successfully a new page. Return true on success, otherwise false.

=cut

sub follow_finishlogin_saml_link {
    my $automaton = shift;

    # Follow the link.
    my $response = $automaton->follow_link('id' => 'finishLoginLink');
    # Otherwise the link was not there and some IDP fault occured.
    if (!$response) {
        my $message = get_saml_status($automaton);
        if (defined $message) {
            printf "SAML IDP reports: $message\n";
        }
        return 0;
    }

    return 1;
}

=head2 do_saml_login($automaton, \%configuration, $prompt_callback)

This private function implements Red Hat SAML IDP client.

Input is the automaton with log-in page. In case of successfull authorization,
output automaton will be back on service provider URL domain. See I<log_in()>
for C<$prompt_callback> description.

Returns true on success, false on failure.

=cut

sub do_saml_login {
    my ($automaton, $configuration, $prompt_callback) = @_;

    # Here we can have three states:
    # (1) A 401 continue form if we had no Kerberos ticket leading to a login
    # form.
    # (2) A 200 hypertext link followed by a continue form if we had
    # a Kerberos ticket and a mutual authentication was requested leading to
    # Orange web.
    # (3) A 200 login form if had a Kerberos ticket but a mutual
    # authentication was not requested (CPAN RT#128699).

    # Folow a finishLoginLink link if this does not look like a login form and
    # the link is there.
    if ($automaton->uri !~ m{\A\Q$configuration->{url}\E} and
            !$automaton->form_name('login_form') and
            $automaton->find_link('id' => 'finishLoginLink')) {
        if (!follow_finishlogin_saml_link($automaton)) {
            return 0;
        }
    }

    # Press a CONTINUE button if this does not look like a login form.
    if ($automaton->uri !~ m{\A\Q$configuration->{url}\E} and
            !$automaton->form_name('login_form') and
            !$automaton->find_link('id' => 'finishLoginLink')) {
        if (!press_continue_saml_button($automaton)) {
            return 0;
        }
    }

    # If we ended up again with a log-in form on the SAML IDP, perform an
    # interactive login.
    if ($automaton->uri !~ m{\A\Q$configuration->{url}\E} and
            $automaton->form_name('login_form')) {
        if (defined $configuration->{samlidp}) {
            if ($automaton->uri !~ m{\A\Q$configuration->{samlidp}\E}) {
                printf("The log-in form does not redirect to " .
                    "the configured SAML identity provider " .
                    "(should point to <%s>, points to <%s>)!\n",
                    $configuration->{samlidp}, $automaton->uri);
                return 0;
            }
        } else {
            printf(
                "Missing SAML identity provider definition in the configuration." .
                " Hope <%s> will not abuse your credentials. " .
                " To silent this warning, set the base URI in the configuration.\n",
                $automaton->uri);
        }

        # Ask for credentials if they have not already been set
        my $site = URI->new($automaton->uri);
        $site->path_query('/');
        if (!defined $configuration->{username}) {
            $configuration->{username} = $prompt_callback->(
                sprintf("User name at <%s>", $site), 0);
            if (!defined $configuration->{username}) {
                print("Log-in procedure canceled.\n");
                return 0;
            }
        }
        if (!defined $configuration->{password}) {
            $configuration->{password} = $prompt_callback->(
                sprintf("Password for `%s' at <%s>",
                    $configuration->{username}, $site), 1);
            if (!defined $configuration->{password}) {
                print("Log-in procedure canceled.\n");
                return 0;
            }
        }

        # Try SAML log-in HTML form
        print "logging in as `", $configuration->{username}, "'\n";
        $automaton->submit_form(
            form_name => 'login_form',
            fields  => {
                'username' => $configuration->{username},
                'password' => $configuration->{password}
            },
        );
        # XXX: IDP returns 200 even in case of bad credentials here.
        # We use press_continue_saml_button() to find a success.

        #dump_state($automaton);

        # Press an expected CONTINUE button
        if (!press_continue_saml_button($automaton)) {
            return 0;
        }
    }

    # Or we ended up back on the Orange web because SAML login was
    # successufully completed.
    if ($automaton->uri !~ m{\A\Q$configuration->{url}\E}) {
        print "The SAML form did not redirect to the ",
            "original service provider <", $configuration->{url},
            "> (points to <", $automaton->uri, ">)!\n";
        return 0;
    }

    return 1;
}


=head2 log_in($automaton, \%configuration, $prompt_callback)

Log into system as defined in %configuration. If user name or password are
needed, C<$prompt_callback> subroutine will be called to obtain the values.

The C<$prompt_callback> expects two arguments: prompt string without a new
line, or any other separator and a boolean indicating if a password is
requested. The function must return the value or C<undef> to dismiss the
request.

Return false in case of failure, true in case of success.

=cut

sub log_in {
    my ($automaton, $configuration, $prompt_callback) = @_;

    # Server refuses requests without Accept: header
    my $headers = $automaton->default_headers;
    $headers->header('Accept' =>
        'text/html,application/xhtml+xml,application/xml');
    $automaton->default_headers($headers);

    print "Getting log-in page for <", $configuration->{url}, ">\n";
    # Disable autocheck because SAML form returns 401 to negotiate Kerberos or
    # to get HTML form filled with OTP credentials.
    $automaton->{'autocheck'} = 0;
    # Request LWP::Authen::Negotiate to peform a mutual authentication,
    # CPAN RT#128699
    local $ENV{LWP_AUTHEN_NEGOTIATE_MUTUAL} = 1;
    $automaton->get($configuration->{url});
    $automaton->{'autocheck'} = 1;

    # SAML phase
    if (!do_saml_login($automaton, $configuration, $prompt_callback)) {
        print "SAML authentication failed!\n";
        return 0;
    }

    if ($automaton->uri !~
            m{/index\.php/leave/redHatViewLeaveList(?:/mode/my)?\z}) {
        printf("Unexpected final URI (got=<%s>)!\n", $automaton->uri);
        return 0;
    }
    return 1;
}


=head2 do_saml_logout($automaton, %configuration)

This private function implements Red Hat SAML IDP client.

Input is the automaton with a log-out page.

Returns true on success, false on failure.

=cut

sub do_saml_logout {
    my ($automaton, $configuration) = @_;

    if (defined $configuration->{samlout}) {
        if ($automaton->uri eq $configuration->{samlout}) {
            return 1;
        } else {
            printf("The log-out form does not redirect to " .
                "the configured SAML logout landing page " .
                "(should point to <%s>, points to <%s>)!\n",
                $configuration->{samlout}, $automaton->uri);
            return 0;
        }
    } else {
        printf(
            "Missing SAML logout landing page definition in the configuration." .
            " Hope <%s> will not abuse your credentials." .
            " To silent this warning, set the URI in the configuration.\n",
            $automaton->uri);
    }

    return 1;
}


=head2 log_out($automaton)

Log out the system.

Return false in case of failure, true in case of success.

=cut

sub log_out {
    my ($automaton, $configuration) = @_;

    print "Logging out\n";
    $automaton->get('/index.php/auth/logOut');

    return do_saml_logout($automaton, $configuration);
}


=head2 time_sheet_year_month($automaton)

Return time identification of current time sheet as (year, month) numbers pair.
E.g. C<(2012, 12)>.
Return undef in case of error or partial list, e.g. C<(2012, undef)>.

=cut

sub time_sheet_year_month {
    my $automaton = shift;

    my $form = $automaton->form_id('searchTimesheet') or return (undef, undef);

    return (
        $form->value('timesheetSearch[yearChoices]'),
        $form->value('timesheetSearch[monthChoices]')
    );
}


=head2 year_month2date($year, $month)

Return year and month formated to string. E.g. C<2012-12>.
Return undef in case of error or partial string, e.g. C<< 2012-<unknown month>
>>.

=cut

sub year_month2date {
    my ($year, $month) = @_;
    if (!defined $year and !defined $month) { return undef; }
    return
        ($year // '<unknown year>') . '-' .  ($month // '<uknown month>');
}


=head2 time_sheet_date($automaton)

Return time identification of current time sheet as string. E.g. C<2012-12>.
Return undef in case of error or partial string, e.g. C<< 2012-<unknown month>
>>.

=cut

sub time_sheet_date {
    return year_month2date(time_sheet_year_month(shift));
}


=head2 time_sheet($automaton)

Retrieve current time sheet.

Return false in case of failure, true in case of success.

=cut

sub time_sheet {
    my $automaton = shift;
    my $path = '/index.php/time/viewTimesheet/mode/my';

    print "Getting time sheet\n";
    $automaton->get($path);

    if ($automaton->uri !~ m{$path\z}) {
        return 0;
    }

    my $maintenance = get_maintenance_status($automaton);
    if (defined $maintenance) {
        print "Time sheet sub-system is not available: $maintenance\n";
        return 0;
    }

    print "Time sheet retrieved successfully.\n";
    return 1;
}


=head2 time_sheet_change($automaton, $year, $month)

Retrieve time sheet for month specified by $year and $month. Month counts from
1, year is in Gregorian calendar.

Return false in case of failure, true in case of success.

=cut

sub time_sheet_change {
    my ($automaton, $year, $month) = @_;
    my $path = '/index.php/time/viewTimesheet(?:/mode/my)?';

    if (!defined $year || !defined $month) {
        return 0;
    }
    if ($automaton->uri !~ m{$path\z}) {
        return 0;
    }

    print "Changing time sheet month to $year-$month\n";
    my $form = $automaton->form_id('searchTimesheet') or return 0;
    $automaton->set_fields(
        'timesheetSearch[yearChoices]' => $year,
        'timesheetSearch[monthChoices]' => $month
    );
    my $submit_button = $form->find_input('#btnSearch') or return 0;
    $automaton->click_button(input => $submit_button);

    if ($automaton->uri !~ m{$path\z}) {
        return 0;
    }

    if (defined (my $message = get_text_status($automaton))) {
        print "Changing time sheet failed: $message\n";
        return 0;
    }
    my $date = time_sheet_date($automaton);
    if (!defined $date or !($date eq "$year-$month")) {
        return 0;
    }

    print "Time sheet changed successfully.\n";
    return 1;
}


=head2 time_sheet_parse($automaton)

Parse current time sheet.

Return undef in case of failure, parsed time sheet as a hash reference in case
of success:

    {
        year    => STRING,
        month   => STRING,
        
        days    => {
            STRING  => {    # day number as a string
                label   => STRING,      # pretty name for the day
                type    => STRING,      # leave, weekend, holiday etc.
                where   => STRING,      # trip, work
                from    => DateTime,    # HH::MM accuracy
                to      => DateTime,    # HH::MM accuracy
                break   => DateTime::Duration,
                doctor  => DateTime::Duration,
                comment => STRING
            },
            ...
        },

        total_entered   => DateTime::Duration,
        total_leave     => DateTime::Duration,
        total_holiday   => DateTime::Duration,
        total_expected  => DateTime::Duration,
        submission_status   => STRING
    }

Any of the values can be C<undef>.

=cut

sub time_sheet_parse {
    my $automaton = shift;
    my $path = '/index.php/time/viewTimesheet(?:/mode/my)?';

    if ($automaton->uri !~ m{$path\z}) {
        return undef;
    }

    my %sheet;

    my $document = $automaton->response->decoded_content;
    my $parser = HTML::TreeBuilder::LibXML->new()->parse_content($document)
        or return undef;
    my $form = $automaton->form_id('editTimesheetForm') or return undef;

    ($sheet{year}, $sheet{month}) = time_sheet_year_month($automaton);
    $sheet{label} = year_month2date($sheet{year}, $sheet{month});
    my $day;
    if (defined $sheet{year} and defined $sheet{month}) {
        $day = DateTime->new(year => $sheet{year}, month => $sheet{month});
    }

    $sheet{total_entered} = DateTime::Duration->new(hours => 0, minutes => 0);

    # Search table body only. It's 2 times faster than whole document;
    my $time_sheet_body = $parser->findnodes(
        q{//table[@id="timesheetTable"]/tbody}
    );
    if ($time_sheet_body->size != 1) { return undef; }
    $time_sheet_body = $time_sheet_body->get_node(0);

    #print "Day\tDayType\tWhere\tFrom\tTo\tBreak\tDoctor\tTotal\tComment\n";
    for (map {$_->name} ($form->inputs)) {
        next if ! defined;
        if (/\Atimesheet\[timesheetRow\]\[([0123456789]+)\]\[date\]\z/) {
            my $index = $1;
            my $day_number = $form->value(
                "timesheet[timesheetRow][$index][date]");
            if ($day_number =~ /\A([0123456789]+)-.*/) {
                $day_number = $1;
            }

            my $sheet_day = $sheet{days}{$day_number} = {};

            # Parse day number
            if (defined $day) {
                # XXX Do not trust $day_number from input.
                eval { $day->set(day => $day_number) };
                if ($@) {
                    print STDERR "Invalid day number `", $day_number, "'\n";
                    ${$sheet_day}{label} = $day_number;
                } else  {
                    ${$sheet_day}{label} = $day->strftime('%d %a');
                }
            } else {
                ${$sheet_day}{label} = $day_number;
            }

            # Parse day type
            {
                my $class = $time_sheet_body->findvalue(
                    q{tr[td/input/@name="timesheet[timesheetRow][} . $index .
                    q{][date]"]/@class}
                ) // '';
                $class =~ s/\AactiveRow\s+//;
                if ($class eq 'days_off') {
                    $class = 'weekend';
                } elsif ($class eq 'full_day_holiday') {
                    $class = 'holiday';
                } elsif ($class eq 'leave_day') {
                    # ??? What is it?
                    $class = 'sickday';
                } elsif ($class eq 'leave_date') {
                    # This is both sickday and leave.
                    # TODO: Discriminate on table[@class=~'leave_info_table']
                    $class = 'leave'
                } elsif ($class eq 'half_day_leave') {
                    $class = 'leave/2'
                }
                ${$sheet_day}{type} = $class;
            }

            # Parse where
            if ($form->value("timesheet[timesheetRow][$index][type]") == 2) {
                ${$sheet_day}{where} = 'trip';
            } else {
                ${$sheet_day}{where} = 'work';
            }

            my $break;
            my $day_is_complete = 1;
            for (qw(inTime outTime breakDuration medicalCheckup totalWork
                comment)) {
                my $value = $form->value("timesheet[timesheetRow][$index][$_]");

                # Parse from, to, break duration to compute totalWork.
                if ($_ eq 'inTime') {
                    if (defined $value and $value ne '') {
                        ${$sheet_day}{from} = string2time($value);
                    } else {
                        $day_is_complete = 0;
                    }
                } elsif ($_ eq 'outTime') {
                    if (defined $value and $value ne '') {
                        ${$sheet_day}{to} = string2time($value);
                    } else {
                        $day_is_complete = 0;
                    }
                } elsif ($_ eq 'breakDuration') {
                    if (defined $value and $value ne '') {
                        ${$sheet_day}{break} = string2time_duration($value, 1);
                        if (defined ${$sheet_day}{break}) {
                            $break = ${$sheet_day}{break}->clone();
                        }
                    } else {
                        $break = DateTime::Duration->new(
                            hours => 0, minutes => 0);
                    }
                } elsif ($_ eq 'medicalCheckup') {
                    if (defined $value and $value ne '') {
                        ${$sheet_day}{doctor} = string2time_duration($value, 1);
                    }
                }

                # Compute totalWork. We cannot trust to the stored value
                # because server does not update it.
                # Accumulate total entered work. Set it to undef if an error
                # occurred.
                elsif ($_ eq 'totalWork') {
                    if (!$day_is_complete) {
                    } elsif (defined ${$sheet_day}{from} and
                            defined ${$sheet_day}{to} and
                            defined $break) {
                        ${$sheet_day}{total} =
                            ${$sheet_day}{to} - ${$sheet_day}{from};
                        ${$sheet_day}{total}->subtract_duration($break);
                        if (defined $sheet{total_entered}) {
                            $sheet{total_entered}->add_duration(
                                ${$sheet_day}{total});
                        }
                    } else {
                        ${sheet}{total_entered} = undef;
                    }
                }
                
                elsif ($_ eq 'comment') {
                    ${$sheet_day}{comment} = $value;
                }
            }
        }
    }


    # Parse duration summary
    # The HTML code contains bogus data. We have to calculate them manually.
    $sheet{total_leave} = string2time_duration(
        $parser->findvalue(q{//*[@id="totalLeave"]/@value}),
        1
    );
    $sheet{total_holiday} = string2time_duration(
        $parser->findvalue(q{//*[@id="totalHoliday"]/@value}),
        1
    );
    $sheet{total_expected} = string2time_duration(
        $parser->findvalue(q{//*[@id="expected"]/@value})
    );

    # Parse submission status
    {
        my $status =
            $parser->findvalue(q{//div[@class="row"]/div[@id="searchPanel"]/div[@id="panelHead"]/text()});
        if (defined $status) {
            $status =~ s/\R/ /s;
            $status =~ s/.*\s+Status:\s*(.*\w)\s*-\s*Country:.*/$1/;
            $sheet{submission_status} = $status;
        }
    }

    return \%sheet;
}


=head2 time_sheet_show($automaton)

Show current time sheet.

Return false in case of failure, true in case of success.

=cut

sub time_sheet_show {
    my $automaton = shift;

    my $sheet = time_sheet_parse($automaton);
    if (! defined $sheet) {
        return 0;
    }

    #dump_state($automaton);

    my $date = year_month2date(${$sheet}{year}, ${$sheet}{month});
    if (defined $date) {
        printf "Time sheet for %s:\n", $date;
    }

    print "Day\tDayType\tWhere\tFrom\tTo\tBreak\tDoctor\tTotal\tComment\n";
    for (sort keys (%{${$sheet}{days}})) {
        my %day = %{${$sheet}{days}{$_}};
        
        print $day{label} // $_ // '';
        print "\t", $day{type} // '';
        print "\t", $day{where} // '';

        printf "\t%s",
            (defined $day{from}) ? time2string($day{from}) : '';

        printf "\t%s",
            (defined $day{to}) ? time2string($day{to}) : '';

        printf "\t%s",
            (defined $day{break}) ? time_duration2string($day{break}) : '';

        printf "\t%s",
            (defined $day{doctor}) ? time_duration2string($day{doctor}) : '';

        printf "\t%s",
            (defined $day{total}) ? time_duration2string($day{total}) : '';

        print "\t", $day{comment} // '';

        print "\n";
    }

    # Show duration summary
    printf "Total holiday: %s\n",
        (defined ${$sheet}{total_holiday}) ?
            time_duration2string(${$sheet}{total_holiday}) : '';
    printf "Total leave: %s\n",
        (defined ${$sheet}{total_leave}) ?
            time_duration2string(${$sheet}{total_leave}) : '';
    printf "Total time entered: %s\n",
        (defined ${$sheet}{total_entered}) ?
            time_duration2string(${$sheet}{total_entered}) : '';
    my $total;
    if (defined ${$sheet}{total_holiday} and defined ${$sheet}{total_leave}
            and defined ${$sheet}{total_entered}) {
        $total = ${$sheet}{total_entered}->clone()->
            add_duration(${$sheet}{total_holiday})->
            add_duration(${$sheet}{total_leave});
    }
    printf "Total time: %s\n",
        (defined $total) ? time_duration2string($total) : '';
    printf "Total time expected: %s\n",
        (defined ${$sheet}{total_expected}) ?
            time_duration2string(${$sheet}{total_expected}) : '';

    # Show submission status
    if (defined ${$sheet}{submission_status}) {
        printf "Status: %s\n", ${$sheet}{submission_status};
    }
    return 1;
}


=head2 normalize_time($time)

Return $time value normalized to HH:MM format.

Return undef if $time is invalid.

This function can differ from web interface.

=cut

sub normalize_time {
    $_ = shift;
    if (!defined) {
        return undef;
    }

    my ($hours, $minutes);
    if (($hours) = (m/\A([0123456789]*)\z/)) {
        $minutes = '00';
    } elsif (!(  ($hours, $minutes) =
            (m/\A([0123456789]*):([0123456789]*)\z/)
        )) {
        print STDERR "Invalid time value `$_'\n";
        return undef;
    }
    
    for ($hours, $minutes) {
        if (length($_) == 0) {
            $_ = '00';
        } elsif (length($_) == 1) {
            $_ = '0' . $_;
        }
    }

    return $hours . ':' . $minutes;
}


=head2 string2time($time)

Return HH:MM $time string as a DateTime object. Only hours and minutes are
meaningful. Other members are dummy.

If $time is invalid, return undef.

This function can differ from web interface.

=cut

sub string2time {
    local $_ = shift;
    if (!defined) {
        return undef;
    }

    my ($hours, $minutes);
    if (($hours) = (m/\A([0123456789]+)\z/)) {
        $minutes = '00';
    } elsif (!(  ($hours, $minutes) =
            (m/\A([0123456789]*):([0123456789]*)\z/)
        )) {
        print STDERR "Invalid time value `$_'\n";
        return undef;
    }

    return DateTime->new(hour => $hours, minute => $minutes,
        year => 1970, month => 1, day => 1);
}


=head2 time2string ($time)

Return DateTime value formated to HH:MM string.

=cut

sub time2string {
    return sprintf('%02d:%02d', $_[0]->hour, $_[0]->minute);
}


=head2 normalize_duration($duration)

Return string $duration value normalized to HH:MM format.

If $duration is invalid, return original value.

This function can differ from web interface.

=cut

sub normalize_duration {
    my ($input) = @_;
    if (!defined $input) {
        return undef;
    }

    my $duration = string2time_duration($input, 0);
    if (ref $duration) {
        return time_duration2string($duration);
    } else {
        return $duration;
    }
}


=head2 string2time_duration($duration, strict)

Return $duration string as DateTime::Duration in HH:MM units.

If $duration is invalid, return original value if $strict is false, return
undef if $strict is true.

This function can differ from web interface.

=cut

sub string2time_duration {
    local $_ = shift;
    my $strict = shift;

    if (!defined) {
        return undef;
    }

    my ($hours, $minutes);
    if (($hours) = (m/\A([0123456789]*)\z/)) {
        $minutes = '00';
    } elsif (!(  ($hours, $minutes) =
            (m/\A([0123456789]*):([0123456789]*)\z/)
        )) {
        print STDERR "Invalid duration value `$_'\n";
        if ($strict) {
            return undef;
        } else {
            return $_;
        }
    }
    
    return DateTime::Duration->new(hours => $hours, minutes => $minutes);
}


=head2 time_duration2string ($duration)

Return DateTime::Duration value formated to HH:MM string.

=cut

sub time_duration2string {
    return sprintf('%02d:%02d', shift->in_units(q{hours}, q{minutes}));
}


=head2 set_if_not_amend_or_defined($amend, $field, $value)

Return list ($field, $value) if $amend is false or $value is defined. Otherwise return empty list.

=cut

sub set_if_not_amend_or_defined {
    my ($amend, $field, $value) = @_;
    (!$amend or defined $value) ? ( $field => $value ) : ();
}


=head2 time_sheet_set($automaton, $day, $from, $to, $break, $doctor,
$comment, $trip, $amend)

This function is deprecated. Please use C<time_sheet_set_day()> followed by
C<time_sheet_save()> instead.

Fill a day into time sheet as if you worked from $from to $to with $break
and $doctor hours. $day of month counts from 1. Time format is HH:MM. $comment
is free text, $trip is true for bussines trip, otherwise standard work.

If #amend is false, the day will be overwritten, i.e undefined arguments will
empty the day records. If $amend is true, only defined arguments will set,
keeping records with undefined arguments untouched.

Return false in case of failure, true in case of success.

=cut

sub time_sheet_set {
    warn "time_sheet_set_day() is deprecated";
    time_sheet_set_day(@_) and time_sheet_save($_[0]);
}


=head2 time_sheet_set_day($automaton, $day, $from, $to, $break, $doctor,
$comment, $trip, $amend)

Fill a day into time sheet as if you worked from $from to $to with $break
and $doctor hours. $day of month counts from 1. Time format is HH:MM. $comment
is free text, $trip is true for bussines trip, otherwise standard work.

If #amend is false, the day will be overwritten, i.e undefined arguments will
empty the day records. If $amend is true, only defined arguments will set,
keeping records with undefined arguments untouched.

Return false in case of failure, true in case of success.

You have to call C<time_sheet_save()> to store modified time sheet into the
server. C<time_sheet_set_day()> does the changes locally only.

=cut

sub time_sheet_set_day {
    my ($automaton, $day, $from, $to, $break, $doctor, $comment, $trip, $amend)
        = @_;
    my $index;
    my $classification;
    if (defined $trip or !$amend) {
        $classification = ($trip ? 2 : 1);
    }
    if (defined $doctor) {
        # Server refuses H:MM $doctor value
        $doctor = normalize_time($doctor);
    }

    print "Filling day #", $day, " into time sheet\n";

    my $form = $automaton->form_id('editTimesheetForm') or return 0;

    # locate day index
    my $day_number = sprintf('%02d', $day);
    for (map {$_->name} ($form->inputs)) {
        next if ! defined;
        if (/\Atimesheet\[timesheetRow\]\[([0123456789]+)\]\[date\]\z/) {
            my $index_candidate = $1;
            my $date = $form->value(
                "timesheet[timesheetRow][$index_candidate][date]");
            if ($date =~ /\A$day_number-.*/) {
                $index = $index_candidate;
                last;
            }
        }
    }
    if (!defined $index) {
        print "Could not locate day #", $day , " in the time sheet\n";
        return 0;
    }

    $automaton->set_fields(
        set_if_not_amend_or_defined($amend,
            "timesheet[timesheetRow][$index][type]" => $classification),
        set_if_not_amend_or_defined($amend,
            "timesheet[timesheetRow][$index][inTime]" => $from),
        set_if_not_amend_or_defined($amend,
            "timesheet[timesheetRow][$index][outTime]" => $to),
        set_if_not_amend_or_defined($amend,
            "timesheet[timesheetRow][$index][breakDuration]" => $break),
        set_if_not_amend_or_defined($amend,
            "timesheet[timesheetRow][$index][medicalCheckup]" => $doctor),
        set_if_not_amend_or_defined($amend,
            "timesheet[timesheetRow][$index][comment]" => $comment),
        # "timesheet[timesheetRow][$index][totalWork]" is not computed by
        # server automatically. Client has to recalculate it. However because
        # any other client can store bogus data, the only solution is to
        # recompute work time duration for each day in show procedure.
    );
    
    print "Day #", $day, " changed.\n";
    return 1;
}


=head2 time_sheet_update_total_times($automaton)

Recalculate totalWork column in a time sheet passed by the argument.

Return true on success, false otherwise.

=cut

sub time_sheet_update_total_times {
    my $automaton = shift;

    print "Updating day total work times\n";

    my $form = $automaton->form_id('editTimesheetForm') or return 0;

    for (map {$_->name} ($form->inputs)) {
        next if ! defined;
        if (/\Atimesheet\[timesheetRow\]\[([0123456789]+)\]\[date\]\z/) {
            my $index = $1;
            my $day_is_complete = 1;
            my ($from, $to, $break, $total);
            for (qw(inTime outTime breakDuration)) {
                my $value = $form->value("timesheet[timesheetRow][$index][$_]");

                # Parse from, to, break duration to compute totalWork.
                if ($_ eq 'inTime') {
                    if (defined $value and $value ne '') {
                        $from = string2time($value);
                    } else {
                        $day_is_complete = 0;
                    }
                } elsif ($_ eq 'outTime') {
                    if (defined $value and $value ne '') {
                        $to = string2time($value);
                    } else {
                        $day_is_complete = 0;
                    }
                } elsif ($_ eq 'breakDuration') {
                    if (defined $value and $value ne '') {
                        $break = string2time_duration($value, 1);
                    } else {
                        $break = DateTime::Duration->new(
                            hours => 0, minutes => 0);
                    }
                }
            }

            # Compute totalWork. We cannot trust to the stored value
            # because server does not update it.
            if ($day_is_complete and
                    defined $from and defined $to and defined $break) {
                $total = time_duration2string(
                    ($to - $from)->subtract_duration($break)
                );
            }
            
            # Rewrite totalWork.
            $automaton->field("timesheet[timesheetRow][$index][totalWork]",
                $total);
        }
    }

    print "Total work times updated.\n";
    return 1;
}


=head2 time_sheet_save($automaton)

Save current time sheet as obtained by C<time_sheet()> or
C<time_sheet_change()> or modified by C<time_sheet_set_day()> into the server.
Total work times are updated automatically.

Return false in case of failure, true in case of success.

=cut

sub time_sheet_save {
    my ($automaton) = @_;

    my $index;
    my $classification;

    # Recompute totalWork
    time_sheet_update_total_times($automaton) or return 0;

    print "Saving time sheet\n";

    my $form = $automaton->form_id('editTimesheetForm') or return 0;

    # Remember time sheet ID to match response URI
    my $time_sheet_id = $form->value('timesheetId');

    #print "DEBUG1: ", $form->dump(), "\n";
    $automaton->set_fields(
        # "timesheet[timesheetRow][$index][totalWork]" is not computed by
        # server automatically. Client has to recalculate it. However because
        # any other client can store bogus data, the only solution is to
        # recompute work time duration for each day in show procedure.
        #
        # This is set by Firefox magically. Probably a JS voodoo.
        "timesheetAction" => 'Save'
    );
    # Server expects disabled values in the form, otherwise it reports failure
    # despite saving the data. Server disables some inputs on full-day
    # leave.
    for (grep {$_->disabled} ($form->inputs)) {
        $_->disabled(0);
    }
    #print "DEBUG2: ", $form->dump(), "\n";
    # WWW::Mechanize cannot find the 'Save' button by value for uknown reason.
    my $submit_button = $form->find_input('#btnSave') or return 0;
    $automaton->click_button(input => $submit_button);

    if ($automaton->uri !~
        m{/index.php/time/viewTimesheet/mode/my/timesheetId/\Q$time_sheet_id\E\z}) {
        print "Wrong response URI\n";
        return 0;
    }
    my $message = get_text_status($automaton);
    if (! defined $message) {
        return 0;
    }
    if (! ($message eq 'Timesheet Successfully Saved') ) {
        print "Saving time sheet failed: $message\n";
        return 0;
    }

    print "Time sheet saved successfully.\n";
    return 1;
}


=head2 time_sheet_submit($automaton, $comment)

Submit current time sheet for review to your manager. You can set submission
$comment. Total work times are updated automatically.

Return false in case of failure, true in case of success.

=cut

sub time_sheet_submit {
    my ($automaton, $comment) = @_;

    # Recompute totalWork
    time_sheet_update_total_times($automaton) or return 0;

    my $date = time_sheet_date($automaton);
    if (defined $date) {
        print "Submitting time sheet for $date\n";
    } else {
        print "Submitting time sheet\n";
    }

    my $form = $automaton->form_id('editTimesheetForm') or return 0;

    # Remember time sheet ID to match response URI
    my $time_sheet_id = $form->value('timesheetId');
    
    $automaton->set_fields(
        "timesheet[submissionComment]" => $comment,
        # JS voodoo:
        "timesheetAction" => 'Submit'
    );
    # Server expects disabled values in the form, otherwise it reports failure
    # despite saving the data. Server disables some inputs on full-day
    # leave.
    for (grep {$_->disabled} ($form->inputs)) {
        $_->disabled(0);
    }
    my $submit_button = $form->find_input('#btnSubmit') or return 0;
    $automaton->click_button(input => $submit_button);

    if ($automaton->uri !~
        m{/index.php/time/viewTimesheet/mode/my/timesheetId/\Q$time_sheet_id\E\z}) {
        print "Wrong response URI\n";
        return 0;
    }
    my $message = get_text_status($automaton);
    if (! defined $message) {
        return 0;
    }
    if (! ($message eq 'Timesheet Successfully Submitted') ) {
        print "Submitting time sheet failed: $message\n";
        return 0;
    }

    print "Time sheet submitted successfully.\n";
    return 1;
}

1;
__END__

=head1 COPYRIGHT

Copyright © 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019  Petr Písař <ppisar@redhat.com>.

=head1 LICENSE

This is free software.  You may redistribute copies of it under the terms of
the GNU General Public License L<http://www.gnu.org/licenses/gpl.html>.
There is NO WARRANTY, to the extent permitted by law.

=cut
