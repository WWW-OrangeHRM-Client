use strict;
use warnings;
use Test::More tests => 15;
use DateTime::Duration;

BEGIN {
    use_ok('WWW::OrangeHRM::Client', qw(string2time_duration));
}

ok(
    !DateTime::Duration->compare(
        string2time_duration('12:34'),
        DateTime::Duration->new(hours=>12, minutes=>34)
    ),
    '12:34'
);
ok(
    !DateTime::Duration->compare(
        string2time_duration('12'),
        DateTime::Duration->new(hours=>12, minutes=>00)
    ),
    '12 hours'
);

ok(
    !DateTime::Duration->compare(
        string2time_duration('01:02'),
        DateTime::Duration->new(hours=>1, minutes=>2)
    ),
    '01:02'
);

ok(
    !DateTime::Duration->compare(
        string2time_duration('1:2'),
        DateTime::Duration->new(hours=>1, minutes=>2)
    ),
    '1:2'
);

is(
    string2time_duration('x1:2'),
    'x1:2',
    'x1:2'
);

is(
    string2time_duration('x1:2', 1),
    undef,
    'strict x1:2'
);

is(
    string2time_duration('1:x2'),
    '1:x2',
    '1:x2'
);

is(
    string2time_duration('1:x2', 1),
    undef,
    'strict 1:x2'
);

is(
    string2time_duration('x'),
    'x',
    'x'
);

is(
    string2time_duration('x', 1),
    undef,
    'strict x'
);

ok(
    !DateTime::Duration->compare(
        string2time_duration(''),
        DateTime::Duration->new(hours=>0, minutes=>0)
    ),
    'empty string'
);

ok(
    !DateTime::Duration->compare(
        string2time_duration('', 1),
        DateTime::Duration->new(hours=>0, minutes=>0)
    ),
    'strict emptry string'
);

is(
    string2time_duration(undef),
    undef,
    'undef'
);

is(
    string2time_duration(undef, 1),
    undef,
    'strict undef'
);

