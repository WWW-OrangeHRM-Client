use strict;
use warnings;
use Test::More tests => 10;
use DateTime;

BEGIN {
    use_ok('WWW::OrangeHRM::Client', qw(string2time));
}

is_deeply(
    string2time('12:34'),
    DateTime->new(hour=>12, minute=>34, year => 1970, month => 1, day => 1),
    '12:34'
);

is_deeply(
    string2time('12'),
    DateTime->new(hour=>12, minute=>00, year => 1970, month => 1, day => 1),
    '12 hours'
);

is_deeply(
    string2time('01:02'),
    DateTime->new(hour=>1, minute=>2, year => 1970, month => 1, day => 1),
    '01:02'
);

is_deeply(
    string2time('1:2'),
    DateTime->new(hour=>1, minute=>2, year => 1970, month => 1, day => 1),
    '1:2'
);

is_deeply(
    string2time('x1:2'),
    undef,
    'x1:2'
);

is_deeply(
    string2time('1:x2'),
    undef,
    '1:x2'
);

is_deeply(
    string2time('x'),
    undef,
    'x'
);

is_deeply(
    string2time(''),
    undef,
    'Emptry string'
);

is_deeply(
    string2time(undef),
    undef,
    'undef'
);

