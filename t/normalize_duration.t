use strict;
use warnings;
use Test::More tests => 10;
use DateTime::Duration;

BEGIN {
    use_ok('WWW::OrangeHRM::Client', qw(normalize_duration));
}

is(
    normalize_duration('12:34'),
    '12:34',
    '12:34'
);

is(
    normalize_duration('12'),
    '12:00',
    '12 hours'
);

is(
    normalize_duration('01:02'),
    '01:02',
    '01:02'
);

is(
    normalize_duration('1:2'),
    '01:02',
    '1:2'
);

is(
    normalize_duration('x1:2'),
    'x1:2',
    'x1:2'
);

is(
    normalize_duration('1:x2'),
    '1:x2',
    '1:x2'
);

is(
    normalize_duration('x'),
    'x',
    'x'
);

is(
    normalize_duration(''),
    '00:00',
    'empty string'
);

is(
    normalize_duration(undef),
    undef,
    'undef'
);

